package anosym.geocoding.provider.google;

import anosym.geocoding.provider.google.config.GoogleMapApiConfigurationService;
import anosym.tests.wiremock.WiremockExtension;
import jakarta.enterprise.context.ApplicationScoped;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 11:04:30 AM
 */
@ApplicationScoped
public class TestGoogleMapApiConfigurationService implements GoogleMapApiConfigurationService {

	@Override
	public String getApiEndpoint() {
		return "http://localhost:" + System.getProperty(WiremockExtension.SERVER_PORT_PROPERTY);
	}

	@Override
	public String getApiKey() {
		return "api-key";
	}

}
