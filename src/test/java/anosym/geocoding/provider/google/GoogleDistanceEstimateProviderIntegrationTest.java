package anosym.geocoding.provider.google;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import anosym.geocoding.DistanceEstimate;
import anosym.geocoding.DistanceEstimateRequest;
import anosym.geocoding.provider.DistanceEstimateProvider;
import anosym.tests.junit.CdiJUnitExtension;
import anosym.tests.wiremock.WiremockExtension;
import jakarta.inject.Inject;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 10:52:47 AM
 */
@Slf4j
@ExtendWith(WiremockExtension.class)
@ExtendWith(CdiJUnitExtension.class)
public class GoogleDistanceEstimateProviderIntegrationTest {

  @Inject
  private DistanceEstimateProvider distanceEstimateProvider;

  @BeforeEach
  @SneakyThrows
  public void setUp() {
    final String jsonResponse = IOUtils.toString(getClass().getResource("/distance-matrix-response.json"), "utf-8");
    stubFor(get(urlPathEqualTo("/maps/api/distancematrix/json"))
            .willReturn(okJson(jsonResponse)));
  }

  @Test
  public void test_resultMatched() {
    final DistanceEstimateRequest request = DistanceEstimateRequest.builder()
            .originAddress("original")
            .destinationsAddress("destination")
            .build();
    final DistanceEstimate estimate = distanceEstimateProvider.estimate(request).orElse(null);
    assertThat(estimate, notNullValue());
    assertThat(estimate, allOf(hasProperty("durationInSeconds", equalTo(340110L)),
                               hasProperty("distanceInMetres", equalTo(1734542L))));
  }

}
