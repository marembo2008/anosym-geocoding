package anosym.geocoding.provider.maprequest;

import anosym.geocoding.provider.maprequest.config.MapRequestApiConfigurationService;
import anosym.tests.wiremock.WiremockExtension;
import jakarta.enterprise.context.ApplicationScoped;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 11:04:30 AM
 */
@ApplicationScoped
public class TestMapRequestApiConfigurationService implements MapRequestApiConfigurationService {

	@Override
	public String getApiEndpoint() {
		return "http://localhost:" + System.getProperty(WiremockExtension.SERVER_PORT_PROPERTY);
	}

	@Override
	public String getApiKey() {
		return "api-key";
	}

}
