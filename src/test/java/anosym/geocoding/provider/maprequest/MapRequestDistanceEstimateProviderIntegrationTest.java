package anosym.geocoding.provider.maprequest;

import static anosym.geocoding.provider.ProviderId.MAP_REQUEST_API;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import anosym.geocoding.DistanceEstimate;
import anosym.geocoding.DistanceEstimateRequest;
import anosym.geocoding.provider.DistanceEstimateProvider;
import anosym.geocoding.provider.Provider;
import anosym.tests.junit.CdiJUnitExtension;
import anosym.tests.wiremock.WiremockExtension;
import jakarta.inject.Inject;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 5:11:40 PM
 */
@Slf4j
@ExtendWith(WiremockExtension.class)
@ExtendWith(CdiJUnitExtension.class)
public class MapRequestDistanceEstimateProviderIntegrationTest {

	@Inject
	@Provider(MAP_REQUEST_API)
	private DistanceEstimateProvider distanceEstimateProvider;

	@BeforeEach
	@SneakyThrows
	public void setUp() {
		final String jsonResponse = IOUtils.toString(getClass().getResource("/direction-api-response.json"), "utf-8");

		log.info("jsonResponse: {}", jsonResponse);

		stubFor(post(urlPathEqualTo("/directions/v2/routematrix")).willReturn(okJson(jsonResponse)));
	}

	@Test
	public void testDirectionApi() {
		final DistanceEstimateRequest request = DistanceEstimateRequest.builder().originAddress("original")
				.destinationsAddress("destination").build();
		final DistanceEstimate estimate = distanceEstimateProvider.estimate(request).orElse(null);
		assertThat(estimate, notNullValue());
		assertThat(estimate, allOf(hasProperty("durationInSeconds", equalTo(1037L)),
				hasProperty("distanceInMetres", equalTo(13052L))));
	}

}
