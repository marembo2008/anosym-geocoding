package anosym.geocoding.provider.config;

import anosym.geocoding.provider.ProviderId;
import anosym.geocoding.provider.config.DistanceEstimateConfigurationService;
import anosym.geocoding.provider.config.LatLngConfigurationService;
import jakarta.enterprise.context.ApplicationScoped;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 4:30:43 PM
 */
@ApplicationScoped
public class TestDistanceEstimateConfigurationService implements DistanceEstimateConfigurationService, LatLngConfigurationService {

  @Override
  public ProviderId getEnabledProvider() {
    return ProviderId.GOOGLE_API;
  }

}
