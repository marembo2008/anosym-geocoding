package anosym.geocoding.provider;

import javax.annotation.Nonnull;

import lombok.RequiredArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 3:34:40 PM
 */
@RequiredArgsConstructor
public enum ProviderId {

  MAP_REQUEST_API("Map Request Api"),
  GOOGLE_API("Google Places Api");

  @Nonnull
  private final String description;

  @Override
  public String toString() {
    return description;
  }

}
