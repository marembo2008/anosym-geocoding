package anosym.geocoding.provider.google;

import javax.annotation.Nonnull;

import anosym.feign.FeignClient;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 6:49:17 AM
 */
@FeignClient("google-map-api")
public interface DistanceMatrixApi {

  @Headers("Accept: application/json")
  @RequestLine("GET /maps/api/distancematrix/json?origins={origins}&destinations={destinations}")
  DistanceEstimateResponse evaluateDistance(@Nonnull @Param("origins") final AddressList originAddresses,
                                            @Nonnull @Param("destinations") final AddressList destinationAddresses);

}
