package anosym.geocoding.provider.google;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 8:53:33 PM
 */
@Data
public class AggregatedRouteResult {

  @JsonProperty("elements")
  private List<RouteEstimate> estimates;

}
