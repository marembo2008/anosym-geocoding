package anosym.geocoding.provider.google;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 7:00:32 AM
 */
@Data
public class ResultValue {

  private Long value;

  private String text;

}
