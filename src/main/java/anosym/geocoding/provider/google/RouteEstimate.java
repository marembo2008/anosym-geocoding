package anosym.geocoding.provider.google;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 7:01:40 AM
 */
@Data
public class RouteEstimate {

  private String status;

  private ResultValue duration;

  private ResultValue distance;

}
