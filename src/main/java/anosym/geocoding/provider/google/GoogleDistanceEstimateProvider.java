package anosym.geocoding.provider.google;

import static anosym.geocoding.provider.ProviderId.GOOGLE_API;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Nonnull;

import anosym.geocoding.DistanceEstimate;
import anosym.geocoding.DistanceEstimateRequest;
import anosym.geocoding.provider.DistanceEstimateProvider;
import anosym.geocoding.provider.Provider;
import jakarta.inject.Inject;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 10:49:25 PM
 */
@Slf4j
@Provider(GOOGLE_API)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GoogleDistanceEstimateProvider implements DistanceEstimateProvider {

  @Inject
  private DistanceMatrixApi distanceMatrixApi;

  @Nonnull
  @Override
  public Optional<DistanceEstimate> estimate(@Nonnull final DistanceEstimateRequest estimateRequest) {
    log.debug("Getting distance matrix from google api");

    try {
      final AddressList origin = AddressList.of(estimateRequest.getOriginAddress());
      final AddressList destination = AddressList.of(estimateRequest.getDestinationsAddress());
      final DistanceEstimateResponse estimateResponse = distanceMatrixApi.evaluateDistance(origin, destination);
      final List<AggregatedRouteResult> routeResult = emptyIfNull(estimateResponse.getResult());
      final Long durationInSeconds = routeResult.stream()
              .map(AggregatedRouteResult::getEstimates)
              .filter(Objects::nonNull)
              .flatMap(List::stream)
              .map(RouteEstimate::getDuration)
              .map(ResultValue::getValue)
              .findFirst()
              .orElse(null);
      final Long distanceInMetres = routeResult.stream()
              .map(AggregatedRouteResult::getEstimates)
              .filter(Objects::nonNull)
              .flatMap(List::stream)
              .map(RouteEstimate::getDistance)
              .map(ResultValue::getValue)
              .findFirst()
              .orElse(null);
      if (durationInSeconds == null && distanceInMetres == null) {
        return Optional.empty();
      }

      return Optional.of(new DistanceEstimate(durationInSeconds, distanceInMetres));
    } catch (final Exception ex) {
      log.error("Error estimating distance from google map api", ex);
      return Optional.empty();
    }
  }

}
