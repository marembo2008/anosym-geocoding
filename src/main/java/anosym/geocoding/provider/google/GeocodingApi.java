package anosym.geocoding.provider.google;

import javax.annotation.Nonnull;

import anosym.feign.FeignClient;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 29, 2020, 6:24:01 PM
 */
@FeignClient("google-map-api")
public interface GeocodingApi {

  @Headers("Accept: application/json")
  @RequestLine("GET /maps/api/geocode/json?address={origins}&destinations={destinations}")
  DistanceEstimateResponse evaluateDistance(@Nonnull @Param("origins") final AddressList originAddresses,
                                            @Nonnull @Param("destinations") final AddressList destinationAddresses);

}
