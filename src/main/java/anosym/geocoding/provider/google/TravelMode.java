package anosym.geocoding.provider.google;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 6:54:30 AM
 */
public enum TravelMode {

  DRIVING,
  WALKING,
  BYCYCLING,
  TRANSIT;

  @Override
  public String toString() {
    return name().toLowerCase();
  }

}
