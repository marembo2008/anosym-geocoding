package anosym.geocoding.provider.google.config;

import javax.annotation.Nonnull;

import anosym.feign.config.FeignConfig;
import anosym.feign.config.FeignConfigProvider;
import jakarta.enterprise.inject.spi.CDI;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 10:39:00 PM
 */
@Slf4j
@FeignConfig("google-map-api")
public class GoogleMapApiFeignConfigProvider implements FeignConfigProvider {

  @Override
  public String getEndPoint(@Nonnull final String feignClientInstanceId) {
    log.info("Getting endpoint for google-map-api for {} api", feignClientInstanceId);

    return CDI.current().select(GoogleMapApiConfigurationService.class).get().getApiEndpoint();
  }

}
