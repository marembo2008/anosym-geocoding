package anosym.geocoding.provider.google.config;

import javax.annotation.Nonnull;

import anosym.feign.FeignClientId;
import anosym.feign.interceptor.ApiRequestInterceptor;
import anosym.feign.interceptor.FeignInterceptor;
import feign.RequestTemplate;
import jakarta.enterprise.inject.spi.CDI;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 10:44:22 PM
 */
@Slf4j
@FeignInterceptor("google-map-api")
public class GoogleMapApiFeignInterceptor implements ApiRequestInterceptor {

  @Override
  public void apply(@Nonnull final FeignClientId feignClientId, @Nonnull final RequestTemplate request) {
    log.debug("Decorating map-api request with api-key for {} api", feignClientId);

    final String apiKey = CDI.current().select(GoogleMapApiConfigurationService.class).get().getApiKey();
    request.query("key", apiKey);
  }

}
