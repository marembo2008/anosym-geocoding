package anosym.geocoding.provider.google.config;

import javax.annotation.Nonnull;

import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 10:34:54 PM
 */
public interface GoogleMapApiConfigurationService {

  @Nonnull
  @Info("The Map api key to access map api services from google")
  String getApiKey();

  @Nonnull
  @Info("Google map api endpoint")
  @Default("https://maps.googleapis.com")
  String getApiEndpoint();

}
