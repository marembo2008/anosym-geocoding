package anosym.geocoding.provider.google;

import static java.util.stream.Collectors.joining;

import javax.annotation.Nullable;

import anosym.feign.expander.ParamExpander;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 11:39:50 AM
 */
public class AddressListParamExpander implements ParamExpander<AddressList> {

  @Override
  public String expand(@Nullable final Object param) {
    if (param == null) {
      return null;
    }

    final AddressList addressList = (AddressList) param;
    return addressList.getAddresses()
            .stream()
            .collect(joining("|"));
  }

}
