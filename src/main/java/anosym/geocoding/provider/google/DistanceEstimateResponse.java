package anosym.geocoding.provider.google;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 8:55:23 PM
 */
@Data
public class DistanceEstimateResponse {

  private String status;

  @JsonProperty("origin_addresses")
  private List<String> originAddresses;

  @JsonProperty("destination_addresses")
  private List<String> destinationAddresses;

  @JsonProperty("rows")
  private List<AggregatedRouteResult> result;

}
