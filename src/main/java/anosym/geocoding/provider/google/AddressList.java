package anosym.geocoding.provider.google;

import java.util.List;

import javax.annotation.Nonnull;

import lombok.AllArgsConstructor;
import lombok.Data;

import static java.util.Collections.singletonList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 11:43:08 AM
 */
@Data
@AllArgsConstructor(staticName = "of")
public class AddressList {

  @Nonnull
  private final List<String> addresses;

  public static AddressList of(@Nonnull final String address) {
    return new AddressList(singletonList(address));
  }

}
