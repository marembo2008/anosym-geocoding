package anosym.geocoding.provider;

import java.util.Optional;

import javax.annotation.Nonnull;

import anosym.geocoding.LatLng;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 29, 2020, 5:53:22 PM
 */
public interface LatLngResolver {

  @Nonnull
  Optional<LatLng> resolve(@Nonnull final String formattedAddress);

}
