package anosym.geocoding.provider;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;

import anosym.geocoding.provider.Provider.ProviderImpl;
import jakarta.enterprise.context.spi.CreationalContext;
import jakarta.enterprise.inject.spi.AnnotatedType;
import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.BeanAttributes;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.enterprise.inject.spi.InjectionTargetFactory;
import lombok.Data;
import org.atteo.classindex.ClassIndex;

import static anosym.geocoding.provider.ProviderId.GOOGLE_API;
import static java.util.stream.Collectors.groupingBy;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 3:38:49 PM
 */
public final class Factory {

  private static final Map<Provider, List<Class<?>>> PROVIDERS;

  static {
    PROVIDERS = ImmutableList
            .copyOf(ClassIndex.getAnnotated(Provider.class))
            .stream()
            .collect(groupingBy((clazz) -> clazz.getAnnotation(Provider.class)));
  }

  private static final Map<Class<?>, FactoryContext<?>> CREATIONAL_CONTEXTS = new ConcurrentHashMap<>();

  @Nonnull
  public static <T> T resolve(@Nonnull final InjectionPoint ip,
                              @Nonnull final BeanManager beanManager,
                              @Nonnull final Class<T> type,
                              @Nonnull final Supplier<ProviderId> enabledProvider) {
    final ProviderId declaredOrDefaultProviderId = firstNonNull(enabledProvider.get(), GOOGLE_API);
    final Provider declaredProvider
            = firstNonNull(ip.getAnnotated().getAnnotation(Provider.class), forId(declaredOrDefaultProviderId));
    final List<Class<?>> providerClasses = emptyIfNull(PROVIDERS.get(declaredProvider));
    final Class<T> actualProviderClass = (Class<T>) providerClasses.stream()
            .filter(type::isAssignableFrom)
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("No provider found for id: " + declaredProvider.value()));
    return (T) CREATIONAL_CONTEXTS
            .computeIfAbsent(actualProviderClass, (clazz) -> getLatLngResolver(clazz, ip, beanManager))
            .getBean();
  }

  @Nonnull
  private static <T> FactoryContext<T> getLatLngResolver(@Nonnull final Class<T> providerClass,
                                                         @Nonnull final InjectionPoint ip,
                                                         @Nonnull final BeanManager beanManager) {
    final AnnotatedType<T> annotatedType = beanManager.createAnnotatedType(providerClass);
    final BeanAttributes<T> beanAttributes = beanManager.createBeanAttributes(annotatedType);
    final InjectionTargetFactory<T> injectionTargetFactory = beanManager.getInjectionTargetFactory(annotatedType);
    final Bean<T> contextual = beanManager.createBean(beanAttributes, providerClass, injectionTargetFactory);
    final CreationalContext<T> creationalContext = beanManager.createCreationalContext(contextual);
    return new FactoryContext(contextual, creationalContext, contextual.create(creationalContext));
  }

  @Nonnull
  private static Provider forId(@Nonnull final ProviderId providerId) {
    return new ProviderImpl(providerId);
  }

  @Data
  private static final class FactoryContext<T> {

    @Nonnull
    private final Bean<T> contextual;

    @Nonnull
    private final CreationalContext<T> creationalContext;

    @Nonnull
    private final T bean;

  }

}
