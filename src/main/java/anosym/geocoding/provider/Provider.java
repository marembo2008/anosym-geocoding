package anosym.geocoding.provider;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.Nonnull;

import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.enterprise.util.Nonbinding;
import jakarta.inject.Qualifier;
import lombok.RequiredArgsConstructor;
import org.atteo.classindex.IndexAnnotated;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 3:39:37 PM
 */
@Qualifier
@IndexAnnotated
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE, FIELD, METHOD, PARAMETER})
public @interface Provider {

  @Nonbinding
  ProviderId value() default ProviderId.GOOGLE_API;

  @RequiredArgsConstructor
  class ProviderImpl extends AnnotationLiteral<Provider> implements Provider {

    @Nonnull
    private final ProviderId value;

    @Override
    public ProviderId value() {
      return value;
    }

  }

}
