package anosym.geocoding.provider.maprequest;

import static anosym.geocoding.provider.ProviderId.MAP_REQUEST_API;
import static java.util.Arrays.asList;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import java.math.BigDecimal;
import java.util.Optional;

import javax.annotation.Nonnull;

import anosym.geocoding.DistanceEstimate;
import anosym.geocoding.DistanceEstimateRequest;
import anosym.geocoding.provider.DistanceEstimateProvider;
import anosym.geocoding.provider.Provider;
import jakarta.inject.Inject;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 3:34:18 PM
 */
@Slf4j
@Provider(MAP_REQUEST_API)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MapRequestDistanceEstimateProvider implements DistanceEstimateProvider {

  @Inject
  private DirectionApi directionApi;

  @Override
  public Optional<DistanceEstimate> estimate(@Nonnull final DistanceEstimateRequest request) {
    log.info("Estimating distance through map-request api: {}", request);

    final RequestOptions requestOptions = new RequestOptions();
    final LocationRequest locationRequest
            = new LocationRequest(asList(request.getOriginAddress(), request.getDestinationsAddress()), requestOptions);
    try {
      final LocationResponse locationResponse = directionApi.evaluateDistance(locationRequest);
      final Long distanceInMetres = emptyIfNull(locationResponse.getDistances())
              .stream()
              .map(value -> value.multiply(BigDecimal.valueOf(1000)).longValue())
              .filter((d) -> d.compareTo(0L) > 0)
              .findFirst()
              .orElse(null);
      final Long durationInSeconds = emptyIfNull(locationResponse.getDurations())
              .stream()
              .filter((d) -> d.compareTo(0L) > 0)
              .findFirst()
              .orElse(null);
      if (distanceInMetres == null && durationInSeconds == null) {
        return Optional.empty();
      }

      return Optional.of(new DistanceEstimate(durationInSeconds, distanceInMetres));
    } catch (final Exception ex) {
      log.error("Error estimating distance from map-request direction-api", ex);
      return Optional.empty();
    }
  }

}
