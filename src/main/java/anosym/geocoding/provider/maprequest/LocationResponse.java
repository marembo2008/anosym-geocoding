package anosym.geocoding.provider.maprequest;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 4:55:48 PM
 */
@Data
public class LocationResponse {

  private boolean allToAll = false;

  private boolean manyToOne = false;

  @JsonProperty("distance")
  private List<BigDecimal> distances;

  @JsonProperty("time")
  private List<Long> durations;

}
