package anosym.geocoding.provider.maprequest;

import java.util.List;

import javax.annotation.Nonnull;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 4:52:17 PM
 */
@Data
public class LocationRequest {

  @Nonnull
  private List<String> locations;

  @Nonnull
  private RequestOptions options;

}
