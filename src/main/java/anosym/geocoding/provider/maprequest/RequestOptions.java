package anosym.geocoding.provider.maprequest;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 4:53:09 PM
 */
@Data
public class RequestOptions {

  private boolean allToAll = false;

  private boolean manyToOne = false;

}
