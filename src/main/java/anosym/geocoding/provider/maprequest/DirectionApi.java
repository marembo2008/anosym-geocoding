package anosym.geocoding.provider.maprequest;

import javax.annotation.Nonnull;

import anosym.feign.FeignClient;
import feign.Headers;
import feign.RequestLine;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 6:49:17 AM
 */
@FeignClient("map-request-api")
public interface DirectionApi {

  @Headers("Accept: application/json")
  @RequestLine("POST /directions/v2/routematrix?inFormat=json&outFormat=json")
  LocationResponse evaluateDistance(@Nonnull final LocationRequest locationRequest);

}
