package anosym.geocoding.provider.maprequest.config;

import javax.annotation.Nonnull;

import anosym.feign.FeignClientId;
import anosym.feign.interceptor.ApiRequestInterceptor;
import anosym.feign.interceptor.FeignInterceptor;
import feign.RequestTemplate;
import jakarta.enterprise.inject.spi.CDI;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 10:44:22 PM
 */
@Slf4j
@FeignInterceptor("map-request-api")
public class MapRequestApiFeignInterceptor implements ApiRequestInterceptor {

  @Override
  public void apply(@Nonnull final FeignClientId feignClientId, @Nonnull final RequestTemplate request) {
    log.debug("Decorating map-request-api request with api-key for {} api", feignClientId);

    final String apiKey = CDI.current().select(MapRequestApiConfigurationService.class).get().getApiKey();
    request.query("key", apiKey);
  }

}
