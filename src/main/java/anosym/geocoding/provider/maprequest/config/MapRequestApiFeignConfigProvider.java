package anosym.geocoding.provider.maprequest.config;

import javax.annotation.Nonnull;

import anosym.feign.config.FeignConfig;
import anosym.feign.config.FeignConfigProvider;
import jakarta.enterprise.inject.spi.CDI;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 10:39:00 PM
 */
@Slf4j
@FeignConfig("map-request-api")
public class MapRequestApiFeignConfigProvider implements FeignConfigProvider {

  @Override
  public String getEndPoint(@Nonnull final String feignClientInstanceId) {
    log.info("Getting endpoint for map-request-api for {} api", feignClientInstanceId);

    return CDI.current().select(MapRequestApiConfigurationService.class).get().getApiEndpoint();
  }

}
