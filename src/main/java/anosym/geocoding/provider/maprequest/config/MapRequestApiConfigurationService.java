package anosym.geocoding.provider.maprequest.config;

import javax.annotation.Nonnull;

import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 10:34:54 PM
 */
public interface MapRequestApiConfigurationService {

  @Nonnull
  @Info("The Map api key to access map request direction apis")
  String getApiKey();

  
  @Nonnull
  @Info("Map request api endpoint")
  @Default("http://www.mapquestapi.com")
  String getApiEndpoint();

}
