package anosym.geocoding.provider.config;

import javax.annotation.Nullable;

import anosym.geocoding.provider.ProviderId;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 3:36:41 PM
 */
public interface DistanceEstimateConfigurationService {

  @Nullable
  ProviderId getEnabledProvider();

}
