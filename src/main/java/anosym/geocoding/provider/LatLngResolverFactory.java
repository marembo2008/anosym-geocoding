package anosym.geocoding.provider;

import javax.annotation.Nonnull;

import anosym.geocoding.provider.config.LatLngConfigurationService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 3:38:49 PM
 */
@ApplicationScoped
public class LatLngResolverFactory {

  @Inject
  private LatLngConfigurationService latLngConfigurationService;

  @Produces
  @Dependent
  public LatLngResolver latLngResolver(@Nonnull final InjectionPoint ip,
                                       @Nonnull final BeanManager beanManager) {
    return Factory.resolve(ip, beanManager, LatLngResolver.class, latLngConfigurationService::getEnabledProvider);
  }

  @Produces
  @Provider
  @Dependent
  public LatLngResolver latLngResolverForQualifier(@Nonnull final InjectionPoint ip,
                                                   @Nonnull final BeanManager beanManager) {
    return Factory.resolve(ip, beanManager, LatLngResolver.class, latLngConfigurationService::getEnabledProvider);
  }

}
