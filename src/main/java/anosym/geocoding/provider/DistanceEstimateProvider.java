package anosym.geocoding.provider;

import java.util.Optional;

import javax.annotation.Nonnull;

import anosym.geocoding.DistanceEstimate;
import anosym.geocoding.DistanceEstimateRequest;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 9:52:13 PM
 */
@FunctionalInterface
public interface DistanceEstimateProvider {

  @Nonnull
  Optional<DistanceEstimate> estimate(@Nonnull final DistanceEstimateRequest estimateRequest);

}
