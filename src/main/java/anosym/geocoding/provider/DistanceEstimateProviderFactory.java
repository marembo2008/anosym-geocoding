package anosym.geocoding.provider;

import javax.annotation.Nonnull;

import anosym.geocoding.provider.config.DistanceEstimateConfigurationService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2019, 3:38:49 PM
 */
@ApplicationScoped
public class DistanceEstimateProviderFactory {

  @Inject
  private DistanceEstimateConfigurationService distanceEstimateConfigurationService;

  @Produces
  @Dependent
  public DistanceEstimateProvider distanceEstimateProvider(@Nonnull final InjectionPoint ip,
                                                           @Nonnull final BeanManager beanManager) {
    return Factory.resolve(ip,
                           beanManager,
                           DistanceEstimateProvider.class,
                           distanceEstimateConfigurationService::getEnabledProvider);
  }

  @Produces
  @Provider
  @Dependent
  public DistanceEstimateProvider distanceEstimateProviderForQualifier(@Nonnull final InjectionPoint ip,
                                                                       @Nonnull final BeanManager beanManager) {
    return Factory.resolve(ip,
                           beanManager,
                           DistanceEstimateProvider.class,
                           distanceEstimateConfigurationService::getEnabledProvider);
  }

}
