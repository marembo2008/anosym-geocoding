package anosym.geocoding;

import java.math.BigDecimal;

import javax.annotation.Nonnull;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 29, 2020, 5:54:16 PM
 */
@Data
public class LatLng {

  @Nonnull
  private BigDecimal lat;

  @Nonnull
  private BigDecimal lng;

}
