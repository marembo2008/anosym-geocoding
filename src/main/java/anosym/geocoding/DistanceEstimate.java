package anosym.geocoding;

import javax.annotation.Nullable;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 9:47:15 PM
 */
@Data
public class DistanceEstimate {

  @Nullable
  private final Long durationInSeconds;

  @Nullable
  private final Long distanceInMetres;

}
