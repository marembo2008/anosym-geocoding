package anosym.geocoding;

import javax.annotation.Nonnull;

import anosym.geocoding.provider.google.TravelMode;
import feign.form.FormProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 30, 2019, 6:51:59 AM
 */
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DistanceEstimateRequest {

  @Nonnull
  @FormProperty("origins")
  private String originAddress;

  @Nonnull
  @FormProperty("destinations")
  private String destinationsAddress;

  @Nonnull
  @Builder.Default
  @FormProperty("mode")
  private TravelMode travelMode = TravelMode.DRIVING;

}
